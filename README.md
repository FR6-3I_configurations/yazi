<div align='center'>

# Yazi

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
pacman -S yazi
```

### ➕ Optional

Official :

```shell
pacman -S \
  ffmpegthumbnailer \
  fd \
  fzf \
  imagemagick \
  mpv \
  p7zip \
  poppler \
  ripgrep
```

## ⚙️ Configuration

Use a [Nerd Font][1] to have icons.

[1]: https://github.com/ryanoasis/nerd-fonts
